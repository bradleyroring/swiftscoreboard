//
//  ViewController.swift
//  scoreboard swift
//
//  Created by Brad Roring on 9/15/17.
//  Copyright (c) 2017 Brad Roring. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //label1
    
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var step1: UIStepper!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var step2: UIStepper!
    

    @IBAction func step1(sender: UIStepper)
    {
        label1.text = String(format:"%.0f", sender.value)
    }

    @IBAction func step2(sender: UIStepper)
    {
        label2.text = String(format:"%.0f", sender.value)
    }
    @IBAction func reset(sender: UIButton)
    {
        label1.text = "0"
        step1.value = 0;
        label2.text = "0"
        step2.value = 0;
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        
        tap.cancelsTouchesInView = false
        
        view.addGestureRecognizer(tap)
  
        
    }

    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

